use std::io::Cursor;

use rocket::{http::Status, response, Request, Response};
//use rocket::response::{Responder};
use serde::Serialize;
use thiserror::Error;

use mongodb::bson;
use validator::ValidationErrors;

#[derive(Error, Debug)]
pub enum Error {
    #[error("mongodb error: {0}")]
    MongoError(#[from] mongodb::error::Error),

    #[error("error during mongodb query: {0}")]
    MongoQueryError(mongodb::error::Error),

    #[error("error during mongodb delete: {0}")]
    MongoDeleteError(mongodb::error::Error),

    #[error("could not access field in document: {0}")]
    MongoDataError(#[from] bson::document::ValueAccessError),

    #[error("error to create an objectid: {0}")]
    MongoObjectIDError(mongodb::bson::oid::Error),

    #[error("Error to encode BSON: {0}")]
    EncodeBSONError(bson::ser::Error),

    #[error("Error to decode BSON: {0}")]
    DecodeBSONError(bson::de::Error),

    #[error("Error to hash password: {0}")]
    HashPasswordError(argon2::Error),

    #[error("Error to generate token")]
    TokenError(jsonwebtoken::errors::Error),

    #[error("Invalid token")]
    InvalidToken(),

    #[error("Invalid S2S secret key")]
    Invalid2S2Key(),

    #[error("User not found")]
    UserNotFound(),

    #[error("Error: {0}")]
    BadRequest(String),

    #[error("Unauthorized")]
    Unauthorized(),

    #[error("Validation: {0}")]
    ValidationError(ValidationErrors),

    #[error("Internal Server Error")]
    InternalServerError,
}

#[derive(Serialize)]
struct ErrorMessage {
    message: String,
    code: u16,
}

impl Error {
    fn is_bad_request(&self) -> bool {
        matches!(self, Error::BadRequest(_))
    }
    fn is_unauthorized(&self) -> bool {
        matches!(self, Error::Unauthorized())
    }
}

impl<'r> rocket::response::Responder<'r, 'static> for Error {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'static> {
        let message = format!("{}", self);
        let mut status = Status::InternalServerError;

        if self.is_bad_request() {
            status = Status::BadRequest;
        } else if self.is_unauthorized() {
            status = Status::Unauthorized;
        }

        let json = serde_json::to_string(&ErrorMessage {
            message,
            code: status.code,
        })
        .unwrap();
        Response::build()
            .header(rocket::http::ContentType::JSON)
            .sized_body(json.len(), Cursor::new(json))
            .status(status)
            .ok()
    }
}
