use rocket::{get, serde::json::Json, State};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    pub salt: String,
    pub secret_key: String,
    pub mongodb_url: String,
    pub mongodb_db: String,
    pub total_max_stats_point: i32,
    pub total_max_equipables_point: i32,
    pub unique_ability: Vec<String>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct PublicConfig {
    total_max_stats_point: i32,
    total_max_equipables_point: i32,

}

#[get("/", format = "json")]
pub async fn get_config(config_state: &State<Config>) -> Json<PublicConfig> {
    let config = PublicConfig {
        total_max_stats_point: config_state.total_max_stats_point,
        total_max_equipables_point: config_state.total_max_equipables_point,
    };
    Json(config)
}
