use std::borrow::Borrow;

use mongodb::{
    bson::{from_document, oid::ObjectId, to_document, Bson, Document},
    options::{
        ClientOptions, DeleteOptions, FindOneOptions, FindOptions, InsertOneOptions,
        UpdateModifications, UpdateOptions,
    },
    results::{DeleteResult, InsertOneResult, UpdateResult},
    Client, Collection, Cursor,
};
use serde::{de::DeserializeOwned, Serialize};

use crate::{config::Config, error::Error};

pub struct DB {
    client: Client,
    db_name: String,
}

impl DB {
    pub async fn new(config: &Config) -> DB {
        let mut client_options = ClientOptions::parse(&config.mongodb_url).await.unwrap();
        client_options.app_name = Some("Backend".to_string());

        DB {
            client: Client::with_options(client_options).unwrap(),
            db_name: config.mongodb_db.clone(),
        }
    }

    #[inline]
    pub async fn find<T>(
        &self,
        collection_name: &str,
        filter: impl Into<Option<Document>>,
        options: impl Into<Option<FindOptions>>,
    ) -> Result<Cursor<T>, Error>
    where
        T: DeserializeOwned + Unpin + Send + Sync,
    {
        Ok(self
            .get_collection::<T>(collection_name)
            .find(filter, options)
            .await
            .map_err(Error::MongoQueryError)?)
    }

    #[inline]
    pub async fn find_one<T>(
        &self,
        collection_name: &str,
        filter: impl Into<Option<Document>>,
        options: impl Into<Option<FindOneOptions>>,
    ) -> Result<Option<T>, Error>
    where
        T: DeserializeOwned + Unpin + Send + Sync,
    {
        Ok(self
            .get_collection::<T>(collection_name)
            .find_one(filter, options)
            .await
            .map_err(Error::MongoQueryError)?)
    }

    #[inline]
    pub async fn insert_one<T>(
        &self,
        collection_name: &str,
        doc: T,
        options: impl Into<Option<InsertOneOptions>>,
    ) -> Result<InsertOneResult, Error>
    where
        T: Borrow<mongodb::bson::Document>,
    {
        Ok(self
            .get_collection(collection_name)
            .insert_one(doc, options)
            .await
            .map_err(Error::MongoQueryError)?)
    }

    #[inline]
    pub async fn update_one(
        &self,
        collection_name: &str,
        query: Document,
        update: impl Into<UpdateModifications>,
        options: impl Into<Option<UpdateOptions>>,
    ) -> Result<UpdateResult, Error> {
        // TODO: Change Document to T
        Ok(self
            .get_collection::<Document>(collection_name)
            .update_one(query, update, options)
            .await
            .map_err(Error::MongoQueryError)?)
    }

    #[inline]
    pub async fn delete_one(
        &self,
        collection_name: &str,
        query: Document,
        options: impl Into<Option<DeleteOptions>>,
    ) -> Result<DeleteResult, Error> {
        Ok(self
            .get_collection::<Document>(collection_name)
            .delete_one(query, options)
            .await
            .map_err(Error::MongoDeleteError)?)
    }

    pub fn get_id_from_document(document: Document) -> Result<String, Error> {
        Ok(document
            .get_object_id("_id")
            .map_err(Error::MongoDataError)?
            .to_hex())
    }

    pub fn get_object_id_from_string(object_id_str: &str) -> Result<ObjectId, Error> {
        ObjectId::parse_str(object_id_str).map_err(Error::MongoObjectIDError)
    }

    pub fn from_document<T>(document: Document) -> Result<T, Error>
    where
        T: DeserializeOwned,
    {
        from_document(document).map_err(Error::DecodeBSONError)
    }

    pub fn to_document<T: ?Sized>(value: &T) -> Result<Document, Error>
    where
        T: Serialize,
    {
        to_document(value).map_err(Error::EncodeBSONError)
    }

    pub fn bson_object_id_to_string(bson: &Bson) -> Result<String, Error> {
        if let Some(object_id) = bson.as_object_id() {
            Ok(object_id.to_hex())
        } else {
            Err(Error::InternalServerError)
        }
    }

    pub fn get_collection<T>(&self, collection_name: &str) -> Collection<T>
    {
        let db = self.client.database(&self.db_name);
        db.collection::<T>(collection_name)
    }
}
