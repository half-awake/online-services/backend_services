pub mod config;
pub mod db;
pub mod error;
pub mod jwt;
pub mod s2s;

use rocket::{routes, Build, Rocket};

pub fn create_route(rocket: Rocket<Build>) -> Rocket<Build> {
    rocket.mount("/api/public/config", routes![config::get_config])
}
