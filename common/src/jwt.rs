use crate::{config::Config, error::Error};
use chrono::Utc;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, TokenData, Validation};
use rocket::{
    http::Status,
    request::{FromRequest, Outcome},
    Request, State,
};
use serde::{Deserialize, Serialize};

const ONE_WEEK: i64 = 60 * 60 * 24 * 7; // in seconds

#[derive(Debug, Serialize, Deserialize)]
pub struct Claims {
    iat: i64,
    pub username: String,
    pub user_id: String,
    exp: i64,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for Claims {
    type Error = Error;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        let config = request
            .guard::<&State<Config>>()
            .await
            .expect("Error to get config state");
        if let Some(auth_header) = request.headers().get_one("Authorization") {
            let auth_header_str = auth_header.to_string();
            if auth_header_str.starts_with("Bearer") {
                let token = auth_header_str[6..auth_header_str.len()].trim();
                if let Ok(token_data) = decode_token(&token.to_string(), &config.secret_key) {
                    return Outcome::Success(token_data.claims);
                }
            }
        }

        Outcome::Failure((Status::Unauthorized, Error::InvalidToken()))
    }
}

pub fn encode_token(username: &str, user_id: String, secret_key: &str) -> Result<String, Error> {
    let now = Utc::now().timestamp_nanos() / 1_000_000_000;
    let claim = Claims {
        iat: now,
        exp: now + ONE_WEEK,
        username: username.to_string(),
        user_id,
    };
    let token = encode(
        &Header::default(),
        &claim,
        &EncodingKey::from_secret(secret_key.as_ref()),
    )
    .map_err(Error::TokenError)?;
    Ok(token)
}

pub fn decode_token(token: &str, secret_key: &str) -> Result<TokenData<Claims>, Error> {
    decode::<Claims>(
        &token,
        &DecodingKey::from_secret(secret_key.as_ref()),
        &Validation::default(),
    )
    .map_err(Error::TokenError)
}
