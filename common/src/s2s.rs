use rocket::{
    http::Status,
    request::{FromRequest, Outcome},
    State,
};

use crate::{config::Config, error::Error};

pub struct S2S {}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for S2S {
    type Error = Error;

    async fn from_request(request: &'r rocket::Request<'_>) -> Outcome<Self, Self::Error> {
        let config = request
            .guard::<&State<Config>>()
            .await
            .expect("Error to get config state");
        if let Some(auth_header) = request.headers().get_one("S2S") {
            let auth_header_str = auth_header.to_string();
            if auth_header_str.starts_with("Key") {
                let key = auth_header_str[3..auth_header_str.len()].trim();
                if key == config.secret_key {
                    return Outcome::Success(S2S {});
                }
            }
        }

        Outcome::Failure((Status::Unauthorized, Error::Invalid2S2Key()))
    }
}
