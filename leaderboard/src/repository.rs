use mongodb::{bson::doc, options::FindOptions, Cursor};
use rocket::futures::TryStreamExt;

use crate::types::{Leaderboard, Leaderboards};
use common::{db::DB, error::Error};

const USER_COLLECTION: &str = "user";

pub struct LeaderboardDB<'a>(pub &'a rocket::State<DB>);

impl LeaderboardDB<'_> {
    pub async fn get_leaderboard(&self) -> Result<Leaderboards, Error> {
        let mut leaderboards: Leaderboards = Leaderboards {
            leaderboards: Vec::new()
        };
        let filter = doc! {};
        let find_option = FindOptions::builder()
            .projection(doc! {"username": 1, "element": 1, "level": 1, "xp": 1})
            .sort(doc! { "level": -1, "xp": -1 })
            .limit(10)
            .build();
        let mut cursor: Cursor<Leaderboard> =
            self.0.find(USER_COLLECTION, filter, find_option).await?;

        while let Some(u) = cursor.try_next().await? {
            leaderboards.leaderboards.push(u);
        }

        Ok(leaderboards)
    }
}
