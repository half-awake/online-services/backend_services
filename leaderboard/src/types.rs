use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Leaderboard {
    pub username: String,
    pub element: String,
    pub level: i16,
    pub xp: i64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Leaderboards {
    pub leaderboards: Vec<Leaderboard>,
}
