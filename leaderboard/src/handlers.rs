use rocket::{get, serde::json::Json, State};

use common::{db::DB, error::Error};

use crate::repository::LeaderboardDB;

use crate::types::Leaderboards;

#[get("/", format = "json")]
pub async fn get_leaderboard(db_state: &State<DB>) -> Result<Json<Leaderboards>, Error> {
    Ok(Json(LeaderboardDB(db_state).get_leaderboard().await?))
}
