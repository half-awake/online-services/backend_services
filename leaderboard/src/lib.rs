mod handlers;
mod repository;
mod types;

use rocket::{routes, Build, Rocket};

pub fn create_route(rocket: Rocket<Build>) -> Rocket<Build> {
    rocket.mount(
        "/api/public/leaderboard",
        routes![handlers::get_leaderboard,],
    )
}