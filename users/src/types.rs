use serde::{Deserialize, Serialize};
use validator::Validate;
use std::{collections::HashMap, fmt};

#[derive(Deserialize, Serialize, Debug, Clone)]
pub enum Element {
    Katon,
    Suiton,
    Futon,
    Kuton,
    Doton,
}
impl fmt::Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
#[derive(Deserialize)]
pub struct EditElement {
    pub element: Element,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Stats {
    pub force: i16,
    pub speed: i16,
    pub health: i16,
    pub mana: i16,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Item {
    pub quantity: i32,
    pub input: String,
}

pub type Inventory = HashMap<String, Item>;
#[derive(Deserialize)]
pub struct UpdateInventory {
    pub inventory: Inventory,
}

pub type Equipables = HashMap<String, i16>;
#[derive(Deserialize, Serialize)]
pub struct UpdateEquipables {
    pub equipables: Equipables,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct User {
    //#[serde(skip_serializing)]
    pub id: String,
    pub username: String,
    #[serde(skip_deserializing)]
    pub password: String,
    pub email: String,
    pub level: i16,
    pub xp: i64,
    pub element: Element,
    pub stats: Stats,
    pub inventory: Inventory,
}
#[derive(Deserialize, Debug)]
pub struct UserLogin {
    pub username: String,
    pub password: String,
}

#[derive(Deserialize, Debug, Validate)]
pub struct UserRegister {
    #[validate(length(min = 3, max = 50))]
    pub username: String,
    #[validate(email)]
    pub email: String,
    #[validate(length(min = 6, max = 150))]
    pub password: String,
    pub password2: String,
    pub element: Element,
}

#[derive(Serialize)]
pub struct TokenLogin {
    pub token: String,
}

#[derive(Serialize, Debug)]
pub struct TokenUser {
    pub valid: bool,
    pub user_name: String,
    pub user_id: String,
    pub client_id: String,
}

#[derive(Serialize, Debug)]
pub struct Token {
    pub token: TokenUser,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct SetLevelAndXP {
    pub level: i32,
    pub xp: i64,
}
