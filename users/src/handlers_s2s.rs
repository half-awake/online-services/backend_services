use common::{db::DB, error::Error, jwt::Claims, s2s::S2S};
use rocket::{get, put, serde::json::Json, State};

use crate::{repository::UserDB, types::{User, SetLevelAndXP}};

#[get("/", format = "json")]
pub async fn get_user(
    _s2s: S2S,
    claims: Claims,
    db_state: &State<DB>,
) -> Result<Json<User>, Error> {
    let user_db = UserDB(db_state);
    Ok(Json(user_db.get_user(&claims.user_id).await?))
}

#[put("/experience", format = "json", data = "<new_level_xp>")]
pub async fn set_level_experience(
    new_level_xp: Json<SetLevelAndXP>,
    _s2s: S2S,
    claims: Claims,
    db_state: &State<DB>,
) -> Result<(), Error> {
    let user_db = UserDB(db_state);
    user_db
        .set_level_experience(&claims.user_id, &new_level_xp)
        .await?;
    Ok(())
}
