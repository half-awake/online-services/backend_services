mod handlers;
mod handlers_s2s;
mod repository;
mod utils;
mod types;

use rocket::{routes, Build, Rocket};

pub fn create_route(rocket: Rocket<Build>) -> Rocket<Build> {
    rocket
        .mount(
            "/api/public/users",
            routes![
                handlers::register,
                handlers::login,
                handlers::me,
                handlers::get_user,
                handlers::validate_token,
                handlers::put_stats,
                handlers::put_inventory,
                handlers::put_equipables,
            ],
        )
        .mount(
            "/api/s2s/users",
            routes![handlers_s2s::get_user, handlers_s2s::set_level_experience],
        )
}

