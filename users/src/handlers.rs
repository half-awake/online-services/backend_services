use std::collections::HashMap;

use crate::{repository::UserDB, types::{Equipables, Inventory, Stats, Token, TokenLogin, TokenUser, UpdateEquipables, UpdateInventory, User, UserLogin, UserRegister}, utils};
use common::{
    config::Config,
    db::DB,
    error::Error,
    jwt::{self, Claims},
};
use rocket::{get, post, put, response::status::Created, serde::json::Json, State};
use validator::Validate;

#[post("/register", format = "json", data = "<user>")]
pub async fn register(
    user: Json<UserRegister>,
    db_state: &State<DB>,
    config_state: &State<Config>,
) -> Result<Created<Json<User>>, Error> {
    if user.password != user.password2 {
        return Err(Error::BadRequest(
            "Password is not the same as the confirmation".to_string(),
        ));
    } else if user.password.len() < 6 {
        return Err(Error::BadRequest(
            "Password must be longer than 6 characters".to_string(),
        ));
    }

    user.validate().map_err(Error::ValidationError)?;

    let user_db = UserDB(db_state);

    user_db.is_user_already_exist(&user).await?;
    user_db.is_email_already_exist(&user).await?;

    let hash = utils::generate_hash_password(&user.password, &config_state.salt)?;

    let stats = Stats {
        force: 5,
        speed: 5,
        health: 5,
        mana: 5,
    };

    let mut new_user = User {
        id: "".to_string(),
        username: user.0.username,
        password: hash.to_string(),
        email: user.0.email,
        level: 1,
        xp: 1,
        element: user.0.element,
        stats,
        inventory: HashMap::new(),
    };

    user_db.create_user(&new_user).await?;

    new_user.password = "".to_string();

    Ok(Created::new("").body(Json(new_user)))
}

#[post("/login", format = "json", data = "<user>")]
pub async fn login(
    user: Json<UserLogin>,
    db_state: &State<DB>,
    config_state: &State<Config>,
) -> Result<Json<TokenLogin>, Error> {
    let user_db = UserDB(db_state);
    let hash = utils::generate_hash_password(&user.password, &config_state.salt)?;
    let user_id = user_db.login(&user, &hash).await?;
    let token = TokenLogin {
        token: jwt::encode_token(&user.username, user_id, &config_state.secret_key)?,
    };
    Ok(Json(token))
}

#[get("/me", format = "json")]
pub async fn me(claims: Claims, db_state: &State<DB>) -> Result<Json<User>, Error> {
    let user_db = UserDB(db_state);
    Ok(Json(user_db.get_user(&claims.user_id).await?))
}

#[get("/<username>", format = "json")]
pub async fn get_user(username: String, db_state: &State<DB>) -> Result<Json<User>, Error> {
    let user_db = UserDB(db_state);
    Ok(Json(user_db.get_user_by_name(&username).await?))
}

#[get("/validate_token", format = "json")]
pub async fn validate_token(claims: Claims) -> Json<Token> {
    let token = Token {
        token: TokenUser {
            valid: true,
            user_name: claims.username,
            user_id: claims.user_id,
            client_id: "1".to_string(),
        },
    };
    Json(token)
}

#[put("/stats", format = "json", data = "<stats>")]
pub async fn put_stats(
    stats: Json<Stats>,
    claims: Claims,
    db_state: &State<DB>,
    config_state: &State<Config>,
) -> Result<Json<Stats>, Error> {
    if stats.force < 0 || stats.speed < 0 || stats.health < 0 || stats.mana < 0 {
        return Err(Error::BadRequest("Stat cannot be less than 0".to_string()));
    }

    let total_stats_point: i32 = (stats.force + stats.speed + stats.health + stats.mana).into();
    if total_stats_point > config_state.total_max_stats_point {
        return Err(Error::BadRequest(format!(
            "You can only put a maximum of {} points of stats on your ninja",
            config_state.total_max_stats_point
        )));
    }

    let user_db = UserDB(db_state);
    user_db.update_stats(&claims.user_id, &stats).await?;

    Ok(stats)
}

#[put("/inventory", format = "json", data="<inventory>")]
pub async fn put_inventory(
    inventory: Json<Inventory>,
    claims: Claims,
    db_state: &State<DB>,
    config_state: &State<Config>,
) -> Result<(), Error> {
    let mut found_ability: Vec<&str> = Vec::new();

    for item in inventory.values() {
        if config_state.unique_ability.contains(&item.input) {
            if found_ability.contains(&&item.input[..]) {
                return Err(Error::BadRequest(format!(
                    "You can only have 1 ability active for {}!",
                    item.input
                )))
            }
            found_ability.push(&item.input);       
        }
    }
    UserDB(db_state).update_inventory(&claims.user_id, &inventory).await?;
    Ok(())
}

#[put("/equipables", format = "json", data = "<equipables>")]
pub async fn put_equipables(
    equipables: Json<UpdateEquipables>,
    claims: Claims,
    db_state: &State<DB>,
) -> Result<(), Error> {
    UserDB(db_state).update_equipables(&claims.user_id, &equipables.equipables).await?;
    Ok(())
}

#[cfg(feature = "cheat")]
#[put("/element", format = "json", data = "<element>")]
pub async fn put_element(
    element: Json<EditElement>,
    claims: Claims,
    db_state: &State<DB>,
) -> Result<(), Error> {
    db_state
        .update_element(&claims.user_id, &element.element)
        .await?;
    Ok(())
}
