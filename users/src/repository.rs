use common::{db::DB, error::Error};
use mongodb::bson::doc;

use crate::types::{Equipables, Inventory};

use super::types::{Element, SetLevelAndXP, Stats, User, UserLogin, UserRegister};

const USER_COLLECTION: &str = "user";

pub struct UserDB<'a>(pub &'a rocket::State<DB>);

impl UserDB<'_> {
    pub async fn is_user_already_exist(&self, user: &UserRegister) -> Result<(), Error> {
        let filter = doc! { "username": &user.username };
        let res: Option<User> = self.0.find_one(USER_COLLECTION, filter, None).await?;
        if res.is_some() {
            return Err(Error::BadRequest(format!(
                "Username {} already used!",
                user.username
            )));
        }
        Ok(())
    }

    pub async fn is_email_already_exist(&self, user: &UserRegister) -> Result<(), Error> {
        let filter = doc! { "email": &user.email };
        let res: Option<User> = self.0.find_one(USER_COLLECTION, filter, None).await?;
        if res.is_some() {
            return Err(Error::BadRequest(format!(
                "Email {} already used!",
                user.email
            )));
        }
        Ok(())
    }

    pub async fn create_user(&self, new_user: &User) -> Result<(), Error> {
        let doc = DB::to_document(&new_user)?;
        let res_insert = self.0.insert_one(USER_COLLECTION, doc, None).await?;

        let user_id = DB::bson_object_id_to_string(&res_insert.inserted_id)?;
        self.0
            .update_one(
                USER_COLLECTION,
                doc! {
                    "_id": &res_insert.inserted_id,
                },
                doc! {
                   "$set": { "id": user_id }
                },
                None,
            )
            .await?;

        Ok(())
    }

    pub async fn login(&self, user: &UserLogin, hash: &str) -> Result<String, Error> {
        let filter = doc![ "username": &user.username, "password": hash ];
        let res = self.0.find_one(USER_COLLECTION, filter, None).await?;

        match res {
            Some(document) => Ok(DB::get_id_from_document(document)?),
            None => {
                return Err(Error::BadRequest(
                    "Incorrect username or password".to_string(),
                ))
            }
        }
    }

    pub async fn get_user(&self, user_id: &str) -> Result<User, Error> {
        let oid = DB::get_object_id_from_string(user_id)?;
        let filter = doc! {"_id": oid};
        let res = self.0.find_one(USER_COLLECTION, filter, None).await?;

        match res {
            Some(document) => {
                let user: User = DB::from_document(document)?;
                return Ok(user);
            }
            None => return Err(Error::UserNotFound()),
        }
    }

    pub async fn get_user_by_name(&self, username: &str) -> Result<User, Error> {
        let filter = doc! { "username": username };
        let res = self.0.find_one(USER_COLLECTION, filter, None).await?;

        match res {
            Some(document) => {
                let user: User = DB::from_document(document)?;
                return Ok(user);
            }
            None => return Err(Error::UserNotFound()),
        }
    }

    pub async fn update_stats(&self, user_id_str: &str, stats: &Stats) -> Result<(), Error> {
        let user_id = DB::get_object_id_from_string(user_id_str)?;
        let stats_doc = DB::to_document(stats)?;
        self.0
            .update_one(
                USER_COLLECTION,
                doc! {
                    "_id": user_id,
                },
                doc! {
                    "$set": { "stats": stats_doc }
                },
                None,
            )
            .await?;

        Ok(())
    }

    pub async fn update_inventory(&self, user_id_str: &str, inventory: &Inventory) -> Result<(), Error> {
        let user_id = DB::get_object_id_from_string(user_id_str)?;
        let inventory_doc = DB::to_document(inventory)?;
        self.0
            .update_one(USER_COLLECTION, doc! { "_id": user_id }, doc! { "$set": { "inventory": inventory_doc }}, None).await?;

        Ok(())
    }

    pub async fn update_equipables(&self, user_id_str: &str, equipables: &Equipables) -> Result<(), Error> {
        let user_id = DB::get_object_id_from_string(user_id_str)?;
        let equipables_doc = DB::to_document(equipables)?;
        self.0
            .update_one(
                USER_COLLECTION,
                doc! {
                    "_id": user_id,
                },
                doc! {
                    "$set": { "equipables": equipables_doc }
                },
                None,
            )
            .await?;
        
        Ok(())
    }

    pub async fn update_element(&self, user_id_str: &str, element: &Element) -> Result<(), Error> {
        let user_id = DB::get_object_id_from_string(user_id_str)?;
        self.0
            .update_one(
                USER_COLLECTION,
                doc! {
                    "_id": user_id,
                },
                doc! {
                    "$set": { "element": element.to_string() }
                },
                None,
            )
            .await?;

        Ok(())
    }

    pub async fn set_level_experience(
        &self,
        user_id_str: &str,
        new_level_xp: &SetLevelAndXP,
    ) -> Result<(), Error> {
        let user_id = DB::get_object_id_from_string(user_id_str)?;

        self.0
            .update_one(
                USER_COLLECTION,
                doc! {
                    "_id": user_id,
                },
                doc! {
                    "$set": {
                        "xp": new_level_xp.xp,
                        "level": new_level_xp.level,
                    }
                },
                None,
            )
            .await?;

        Ok(())
    }
}
