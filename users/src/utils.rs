use argon2::{self, Config};

use common::error::Error;

pub fn generate_hash_password(password: &str, salt: &str) -> Result<String, Error> {
    let config = Config::default();
    let hash = argon2::hash_encoded(password.as_bytes(), salt.as_bytes(), &config)
        .map_err(Error::HashPasswordError)?;
    Ok(hash)
}
