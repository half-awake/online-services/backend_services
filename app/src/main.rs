use common::{config::Config, db::DB};

#[macro_use]
extern crate rocket;

#[launch]
async fn rocket() -> _ {
    let rocket = rocket::build();
    let figment = rocket.figment();

    let config: Config = figment.extract().expect("Error to parse config file");

    let db = DB::new(&config).await;

    let rocket = common::create_route(users::create_route(leaderboard::create_route(
        sessions::create_route(rocket),
    )));
    rocket.manage(db).manage(config)
}
