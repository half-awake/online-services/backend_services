use std::net::IpAddr;

use common::{db::DB, error::Error};
use mongodb::{bson::doc, options::FindOptions, Cursor};
use rocket::futures::TryStreamExt;

use crate::types::{NewSession, Session, Sessions};

const SESSION_COLLECTION: &str = "session";

pub struct SessionDB<'a>(pub &'a rocket::State<DB>);

// I think it's a better idea to save session in reddis :)
impl SessionDB<'_> {
    pub async fn create_session(&self, new_session: NewSession) -> Result<(), Error> {
        let session: Session = Session {
            name: new_session.name,
            description: new_session.description,
            map: new_session.map,
            ip: new_session.ip,
            is_public: new_session.is_public,
            max_num_players: new_session.max_num_players,
            updated_at: mongodb::bson::DateTime::now(),
            players: 0,
        };

        let doc = DB::to_document(&session)?;
        self.0.insert_one(SESSION_COLLECTION, doc, None).await?;

        Ok(())
    }

    pub async fn is_session_name_already_used(&self, name: &str) -> Result<(), Error> {
        let filter = doc! { "name": name };
        let res: Option<Session> = self.0.find_one(SESSION_COLLECTION, filter, None).await?;
        if res.is_some() {
            return Err(Error::BadRequest(format!(
                "Session name {} already used!",
                name
            )));
        }

        Ok(())
    }

    pub async fn is_session_ip_already_used(&self, ip: &IpAddr) -> Result<(), Error> {
        let filter = doc! { "ip": ip.to_string() };
        let res: Option<Session> = self.0.find_one(SESSION_COLLECTION, filter, None).await?;
        if res.is_some() {
            return Err(Error::BadRequest(format!(
                "Session ip {} already used!",
                ip
            )));
        }

        Ok(())
    }

    pub async fn delete_session(&self, name: &str) -> Result<(), Error> {
        let result = self
            .0
            .delete_one(SESSION_COLLECTION, doc! { "name": name }, None)
            .await?;

        if result.deleted_count == 0 {
            return Err(Error::BadRequest(format!(
                "There is no session with '{}' name",
                name
            )));
        }
        Ok(())
    }

    pub async fn update_session_players(
        &self,
        session_name: &str,
        players: i32,
    ) -> Result<(), Error> {
        let result = self
            .0
            .update_one(
                SESSION_COLLECTION,
                doc! {
                    "name": session_name,
                },
                doc! {
                    "$set": { "players": players, "updatedAt": mongodb::bson::DateTime::now() }
                },
                None,
            )
            .await?;

        if result.modified_count == 0 {
            return Err(Error::BadRequest(format!(
                "There is no session with '{}' name",
                session_name
            )));
        }

        Ok(())
    }

    pub async fn get_sessions(&self) -> Result<Sessions, Error> {
        let mut sessions = Sessions {
            sessions: Vec::new(),
        };
        let filter = doc! {};
        let find_option = FindOptions::builder()
            .sort(doc! { "players": -1 })
            .limit(20) // TODO: add a pagination system
            .build();

        let mut cursor: Cursor<Session> =
            self.0.find(SESSION_COLLECTION, filter, find_option).await?;

        while let Some(s) = cursor.try_next().await? {
            sessions.sessions.push(s);
        }

        Ok(sessions)
    }

    pub async fn ping_session(&self, session_name: &str) -> Result<(), Error> {
        let result = self
            .0
            .update_one(
                SESSION_COLLECTION,
                doc! {
                    "name": session_name,
                },
                doc! {
                    "$set": { "updatedAt": mongodb::bson::DateTime::now() }
                },
                None,
            )
            .await?;

        if result.modified_count == 0 {
            return Err(Error::BadRequest(format!(
                "There is no session with '{}' name",
                session_name
            )));
        }

        Ok(())
    }
}
