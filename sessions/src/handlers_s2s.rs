use rocket::{delete, get, post, put, serde::json::Json, State};

use common::{db::DB, error::Error, s2s::S2S};

use crate::repository::SessionDB;

use crate::types::NewSession;

#[post("/", format = "json", data = "<new_session>")]
pub async fn create_session(
    new_session: Json<NewSession>,
    _s2s: S2S,
    db_state: &State<DB>,
) -> Result<(), Error> {
    let session_db = SessionDB(db_state);

    session_db
        .is_session_name_already_used(&new_session.name)
        .await?;
    session_db
        .is_session_ip_already_used(&new_session.ip)
        .await?;

    session_db.create_session(new_session.0).await?;
    Ok(())
}

#[delete("/<session_name>")]
pub async fn delete_session(
    session_name: &str,
    _s2s: S2S,
    db_state: &State<DB>,
) -> Result<(), Error> {
    Ok(SessionDB(db_state).delete_session(session_name).await?)
}

#[put("/players/<session_name>/<players>")]
pub async fn update_session_players(
    session_name: &str,
    players: i16,
    _s2s: S2S,
    db_state: &State<DB>,
) -> Result<(), Error> {
    Ok(SessionDB(db_state)
        .update_session_players(session_name, players.into())
        .await?)
}

#[get("/ping/<session_name>")]
pub async fn ping_session(
    session_name: &str,
    _s2s: S2S,
    db_state: &State<DB>,
) -> Result<(), Error> {
    Ok(SessionDB(db_state).ping_session(session_name).await?)
}
