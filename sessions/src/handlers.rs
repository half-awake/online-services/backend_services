use rocket::{get, serde::json::Json, State};

use common::{db::DB, error::Error};

use crate::repository::SessionDB;

use crate::types::Sessions;

#[get("/", format = "json")]
pub async fn all_sessions(db_state: &State<DB>) -> Result<Json<Sessions>, Error> {
    Ok(Json(SessionDB(db_state).get_sessions().await?))
}
