use serde::{Deserialize, Serialize};
use std::net::IpAddr;

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Session {
    pub name: String,
    pub description: String,
    pub map: String,
    pub ip: IpAddr,
    pub is_public: bool,
    pub max_num_players: i16,
    pub players: i16,
    pub updated_at: mongodb::bson::DateTime,
}

#[derive(Deserialize, Serialize, Debug)]
pub struct Sessions {
    pub sessions: Vec<Session>,
}

#[derive(Deserialize, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct NewSession {
    pub name: String,
    pub description: String,
    pub map: String,
    pub ip: IpAddr,
    pub is_public: bool,
    pub max_num_players: i16,
}
