mod handlers;
mod handlers_s2s;
mod repository;
mod types;

use rocket::{routes, Build, Rocket};

pub fn create_route(rocket: Rocket<Build>) -> Rocket<Build> {
    rocket
        .mount("/api/public/sessions", routes![handlers::all_sessions,])
        .mount(
            "/api/s2s/sessions",
            routes![
                handlers_s2s::create_session,
                handlers_s2s::delete_session,
                handlers_s2s::update_session_players,
                handlers_s2s::ping_session,
            ],
        )
}
